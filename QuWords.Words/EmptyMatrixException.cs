﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuWords.Words
{
    public class EmptyMatrixException : Exception
    {
        public EmptyMatrixException() : base("The matrix should have at least one row")
        { 
        }
    }

    public class UnevenRowLengthException : Exception
    {
        public UnevenRowLengthException() : base("The matrix should have the same lenth in every row")
        {
        }
    }

    public class MaxSizeExceededException : Exception
    {
        public MaxSizeExceededException() : base("The size of the given matrix exceeded the maximun size")
        {
        }
    }
}
