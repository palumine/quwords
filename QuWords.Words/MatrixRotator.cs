﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuWords.Words
{
    public class MatrixRotator
    {
        /// <summary>
        /// Rotates a List of same length strings so the first letter of every string creates the first item of the resultant list.
        /// The second letter of every string createas the second item and so on. 
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public virtual IEnumerable<String> Rotate(IEnumerable<string> matrix)
        {
            var newMatrix = new List<string>();
            var wordLenght = matrix.First().Length;
            var newRow = string.Empty;

            for (var i = 0; i < wordLenght; i++)
            {
                newRow = matrix.Aggregate(string.Empty, (current, next) => string.Concat(current, next[i]));
                newMatrix.Add(newRow);
            }

            return newMatrix;
        }
    }
}
