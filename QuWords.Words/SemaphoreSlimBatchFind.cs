﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuWords.Words
{
    /// <summary>
    /// Implementation of IBatchFind using Parallel.Foreach
    /// 
    /// Its actually forcing the use of more than one thread
    /// </summary>
    public class SemaphoreSlimBatchFind : IBatchFind
    {

        private ILogger<SemaphoreSlimBatchFind> Logger { get; }
        public SemaphoreSlimBatchFind(ILogger<SemaphoreSlimBatchFind> logger)
        {
            this.Logger = logger;
        }

        public async Task<IEnumerable<IEnumerable<(string word, int count)>>> BatchFindAsync(
            Func<IEnumerable<string>, IEnumerable<string>, IEnumerable<(string word, int count)>> findFunc,
            IEnumerable<string> matrix,
            IEnumerable<IEnumerable<string>> batchList)
        {
            this.Logger?.LogInformation($"Starting SemaphoreSlimBatchFind");
            var result = new List<IEnumerable<(string word, int count)>>();

            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            var batchCount = 0;
            await Task.Run(() =>
            {
                using (var semaphore = new SemaphoreSlim(Environment.ProcessorCount))
                {
                    var tasks = batchList.Select(batch =>

                        Task.Run(() =>
                        {
                            semaphore.Wait();
                            try
                            {
                                result.Add(findFunc(matrix, batch));
                                batchCount++;
                            }
                            finally
                            {
                                semaphore.Release();
                            }
                        })
                    );
                    Task.WaitAll(tasks.ToArray());
                }
            }
            );
            timer.Stop();
            this.Logger?.LogInformation($"Finishing SemaphoreSlimBatchFind in {timer.Elapsed.Seconds}.{timer.Elapsed.Milliseconds} seconds. {batchCount} batches");
            return result;

        }

    }
}
