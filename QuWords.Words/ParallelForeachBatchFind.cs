﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuWords.Words
{
    /// <summary>
    /// Implementation of IBatchFind using Parallel.Foreach
    /// 
    /// Parallel foreach optimize the amount of threads that uses, so is it could be using one or more threads.
    /// </summary>
    public class ParallelForeachBatchFind : IBatchFind
    {
        public ILogger<ParallelForeachBatchFind> Logger { get; }
        public ParallelForeachBatchFind(ILogger<ParallelForeachBatchFind> logger)
        {
            Logger = logger;
        }



        public async Task<IEnumerable<IEnumerable<(string word, int count)>>> BatchFindAsync(
            Func<IEnumerable<string>, IEnumerable<string>, IEnumerable<(string word, int count)>> findFunc,
            IEnumerable<string> matrix,
            IEnumerable<IEnumerable<string>> batchList)
        {
            this.Logger?.LogInformation($"Starting ParallelForeachBatchFind");
            var result = new List<IEnumerable<(string word, int count)>>();

            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            var batchCount = 0;
            await Task.Run(() =>
                Parallel.ForEach(batchList, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, //Trying to use all the processors
                batch =>
                {
                    batchCount++;
                    result.Add(findFunc(matrix, batch));
                }));
            timer.Stop();
            this.Logger?.LogInformation($"Finishing ParallelForeachBatchFind in {timer.Elapsed.Seconds}.{timer.Elapsed.Milliseconds} seconds. {batchCount} batches");

            return result;
        }

    }
}
