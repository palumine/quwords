﻿using BenchmarkDotNet.Attributes;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuWords.Words
{
    /// <summary>
    /// Implementation of IBatchFind using Parallel.Foreach
    /// </summary>
    public class SequentialBatchFind : IBatchFind
    {
        public ILogger<SequentialBatchFind> Logger { get; }

        public SequentialBatchFind()
        {
        }
        public SequentialBatchFind(ILogger<SequentialBatchFind> logger)
        {
            Logger = logger;
        }

        [Benchmark]
        public async Task<IEnumerable<IEnumerable<(string word, int count)>>> BatchFindAsync(
            Func<IEnumerable<string>, IEnumerable<string>, IEnumerable<(string word, int count)>> findFunc,
            IEnumerable<string> matrix,
            IEnumerable<IEnumerable<string>> batchList)
        {
            this.Logger?.LogInformation($"Starting SequentialBatchFind");
            var result = new List<IEnumerable<(string word, int count)>>();

            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            var batchCount = 0;

            await Task.Run(() =>
            {
                foreach (var batch in batchList)
                {
                    result.Add(findFunc(matrix, batch));
                    batchCount++;
                }
            });

            timer.Stop();
            this.Logger?.LogInformation($"Finishing SequentialBatchFind in {timer.Elapsed.Seconds}.{timer.Elapsed.Milliseconds} seconds. {batchCount} batches Processed");
            return result;
        }

    }
}
