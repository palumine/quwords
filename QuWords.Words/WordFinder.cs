﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuWords.Words
{
    public class WordFinder
    {
        private const int MAX_SIZE = 64;

        /// <summary>
        /// Gets the Matrix
        /// </summary>
        protected IEnumerable<string> Matrix { get; }
        public ILogger<WordFinder> Logger { get; }

        private MatrixRotator _matrixRotator;
        /// <summary>
        /// Gets the matrix rotator
        /// </summary>
        private MatrixRotator MatrixRotator
        {
            get
            {
                return _matrixRotator ??= new MatrixRotator();
            }
        }

        /// <summary>
        /// Get or sets the Batch Find to change the way batches are managed
        /// </summary>
        public IBatchFind BatchFind { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="matrix">Set of strings</param>
        public WordFinder(IEnumerable<string> matrix)
        {
            this.Matrix = matrix ?? throw new ArgumentNullException(nameof(matrix));

            if (!matrix.Any())
                throw new EmptyMatrixException();

            if (matrix.Count() > MAX_SIZE || matrix.Any(row => row.Length > MAX_SIZE))
                throw new MaxSizeExceededException();

            var rowSize = matrix.First().Length;
            if (matrix.Any(row => row.Length != rowSize))
                throw new UnevenRowLengthException();

            this.BatchFind = new SequentialBatchFind();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="matrix">Set of strings</param>
        public WordFinder(IEnumerable<string> matrix, ILogger<WordFinder> logger) : this(matrix)
        {
            this.Logger = logger;
        }

        /// <summary>
        /// Finds the words in wordstream in the instance matrix. 
        /// In case of same amount of ocurrencies for different words it does not guarantes wich words are included
        /// and which words are left out of the Top 10.
        /// </summary>
        /// <param name="wordstream">words to find</param>
        /// <returns>Top 10 ocurrencies each word in wordstream</returns>
        public IEnumerable<string> Find(IEnumerable<string> wordstream)
        {
            var wordstreamWithoutDuplicates = new List<string>(wordstream.Distinct()); //Remove duplicates

            var rotatedMatrix = this.Rotate(this.Matrix);

            var batchesResult = FindInBatches(this.Matrix, rotatedMatrix, wordstreamWithoutDuplicates);

            var joinedResults = JoinResults(batchesResult.Result);

            return CollectTopTen(joinedResults);
        }

        /// <summary>
        /// Joins the results of the processed batches
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        private static IEnumerable<(string, int)> JoinResults(IEnumerable<IEnumerable<(string word, int count)>> results)
        {
            var totalizedResults = results.SelectMany(r => r)
                .GroupBy(wordCount => wordCount.word)
                .Select(wordGroup => (word: wordGroup.Key, count: wordGroup.Sum(wordCount => wordCount.count)));
            //Joins all result sets to totalize ocurrencies

            return totalizedResults;
        }


        private static IEnumerable<string> CollectTopTen(IEnumerable<(string word, int count)> wordCountings)
        {
            var where = wordCountings.Where(word => word.count > 0);//Filters only the finded words
            var orderBy = where.OrderByDescending(word => word.count); //Ordering by count
            var take = orderBy.Take(10); //Take top 10
            var select = take.Select(word => word.word);//Selects only the word
            return select.ToList();
        }

        /// <summary>
        /// Searchs every word of the given wordstrem in every row of the given matrix 
        /// </summary>
        /// <param name="matrix">Matrix</param>
        /// <param name="wordstream">Words to find</param>
        /// <returns>Returns a set of couples, with the number ocurrences of each word in the matrix</returns>
        protected async virtual Task<IEnumerable<IEnumerable<(string word, int count)>>> FindInBatches(
            IEnumerable<string> matrix,
            IEnumerable<string> rotatedMatrix,
            IEnumerable<string> wordstream)
        {
            var batchList = SplitList(wordstream, 100000);
            var result = new List<IEnumerable<(string word, int count)>>();
            //async execution to find the wordstream in the matrix and in the rotated matrix

            var taskMatrix = this.BatchFind.BatchFindAsync((m, b) => Find(m, b), matrix, batchList);
            var taskRotatedMatrix = this.BatchFind.BatchFindAsync((m, b) => Find(m, b), rotatedMatrix, batchList);


            await Task.WhenAll(taskMatrix, taskRotatedMatrix);

            result.AddRange(taskMatrix.Result);
            result.AddRange(taskRotatedMatrix.Result);

            return result;
        }

        /// <summary>
        /// Splits a list in sublists of size nSize
        /// </summary>
        /// <typeparam name="T">Type of the elements of the list</typeparam>
        /// <param name="wordstream">List to split</param>
        /// <param name="nSize">Size of every batch</param>
        /// <returns></returns>
        private IEnumerable<IEnumerable<T>> SplitList<T>(IEnumerable<T> wordstream, int nSize)
        {
            var parts = new List<IEnumerable<T>>();
            this.Logger?.LogInformation("Split init");
            var i = 0;
            var part = wordstream.Skip(i * nSize).Take(nSize);
            while (part.Any())
            {
                parts.Add(part);
                i++;
                part = wordstream.Skip(i * nSize).Take(nSize);
            }
            this.Logger?.LogInformation("Split end");
            return parts;
        }

        /// <summary>
        /// Searchs every word of the given wordstrem in every row of the given matrix 
        /// </summary>
        /// <param name="matrix">Matrix</param>
        /// <param name="wordstream">Words to find</param>
        /// <returns>Returns a set of couples, with the number ocurrences of each word in the matrix</returns>
        protected virtual IEnumerable<(string word, int count)> Find(IEnumerable<string> matrix,
            IEnumerable<string> wordstream)
        {
            this.Logger?.LogInformation($"Thread: {Thread.CurrentThread.ManagedThreadId} Find init");
            var findResult = new List<(string word, int count)>();
            foreach (var word in wordstream)
            {
                var count = 0;
                foreach (var row in matrix)
                {
                    if (row.Contains(word))
                    {
                        count++;
                    }
                }
                if (count > 0)
                {
                    findResult.Add((word, count));
                }
            }
            this.Logger?.LogInformation($"Find end");

            return findResult;
        }

        /// <summary>
        /// Rotates the given set of strings
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        protected virtual IEnumerable<string> Rotate(IEnumerable<string> matrix)
        {
            return this.MatrixRotator.Rotate(matrix);
        }
    }
}
