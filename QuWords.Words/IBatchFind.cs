﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuWords.Words
{
    /// <summary>
    /// Executes multiple searches in batches
    /// Implementations of this interface chooses how to manage the batch List.
    /// </summary>
    public interface IBatchFind
    {
        /// <summary>
        /// Finds every batch of wordstream in the matrix. 
        /// </summary>
        /// <param name="findFunc">Function used to find the wordstream in the matrix</param>
        /// <param name="matrix">matrix</param>
        /// <param name="batchList">List of batches of wordstream to find</param>
        /// <returns></returns>
        public Task<IEnumerable<IEnumerable<(string word, int count)>>> BatchFindAsync(
            Func<IEnumerable<string>, IEnumerable<string>, IEnumerable<(string word, int count)>> findFunc,
            IEnumerable<string> matrix,
            IEnumerable<IEnumerable<string>> batchList);

    }
}
