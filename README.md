# README #

* To run these tests:
	1. Using Visual Studio Test Explorer. 
	2. dotnet test QuWords.Words.Test
	3. Using testcentric-gui

### What is this repository for? ###

* Qu Test to .Net Sr Developer position.

### How do I get set up? ###

* I run this test primarly using Visual Studio Tools.

### Tests considerations ###
* There are two different Test. 
	* Functional Tests: I tried to cover all scenarios. There are data classes that represent them.
	* Performance Tests: I did one test for each batch processing strategy to compare bechmark results.
		* For the performance test data, I generate 64x64 matrix with random chars and I extract all possible words from it.
		* There is a maximum amount of 133120 possible words of 1 to 64 length horizontally and 133120 words vertically without considering duplicate words
	
### Batch processing strategies ###
* I tried 3 strategies:
	* Sequential: Just executes one batch after the other without parallel execution
	* ParallerForeach: Uses Parallel.Foreach pattern to executes every batch. The MaxDegreeOfParallelism can be modified by code.
	* SemaphoreSlim: Uses SempahoreSlim to executes every batch. The intialCount (number of concurrent requests) can be modified by code.
	
	Every strategy is called with a delegate function that executes the Find method in the WordFind class. 
	So the comparisson is about the batch execution strategy and not about the Finding process.
	
### Horizontal and vertical search ###
* I'm searching within the matrix in the horizontal orientation first. Then I rotate the matrix and execute the same search to find the vertical occurrencies.
* This two search are independent from each other, so they are executed in parallel with Parallel.Foreach

## Class Diagram ##

![](https://bitbucket.org/palumine/quwords/raw/d75b2d948cdc1470eacccc46431be91798e68711/Images/ClassDiagram.jpg)


### Cores and tests results ###
* My CPU only has 2 physical cores and 4 logical cores. 
* To compare performance between strategies, executing horizontal and vertical search sequentially gives more convincing results because the threads are dedicated to the batches processing.
* This test are executed with a wordstream length of 250000+ non duplicated words

#### Sequential ####
* In this cases, it awaits each task. After that, results are collected.

``` csharp
    var matrixResult = await this.BatchFind.BatchFindAsync((m, b) => Find(m, b), matrix, batchList);
    var rotatedMatrixResult = await this.BatchFind.BatchFindAsync((m, b) => Find(m, b), rotatedMatrix, batchList);

    result.AddRange(matrixResult);
    result.AddRange(rotatedMatrixResult);
```


![Test result when Horizontal and Vertical search is sequential](https://bitbucket.org/palumine/quwords/raw/f480a9cd74566e1a473ca60da6db704a46c22e62/Images/TestResult.SequentialHandV.jpg)


#### Async ####
* In this case both tasks start simultaneously. When both end results are collected.

``` csharp
    var taskMatrix = this.BatchFind.BatchFindAsync((m, b) => Find(m, b), matrix, batchList);
    var taskRotatedMatrix = this.BatchFind.BatchFindAsync((m, b) => Find(m, b), rotatedMatrix, batchList);

    await Task.WhenAll(taskMatrix, taskRotatedMatrix);
            
    result.AddRange(taskMatrix.Result);
	result.AddRange(taskRotatedMatrix.Result);
```

![Test result when Horizontal and Vertical search is async](https://bitbucket.org/palumine/quwords/raw/47829943e9f40dd485490931e9567f43d69cb755/Images/TestResutl.AsyncHandV.jpg)


### More Tests ###

* I tried using a larger matrix of 128x128, that can produce 1500000+ non duplicated words, and it shows that the SemaphoreSlim strategy performs better in every case.







	
	






