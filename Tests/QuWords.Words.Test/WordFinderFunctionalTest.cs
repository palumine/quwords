﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QuWords.Words.Test
{
    [TestFixture]
    public class WordFinderFunctionalTest
    {
        [Test]
        public void IfTheMatrixIsEmptyConstructorShouldFail()
        {
            Assert.Throws(typeof(EmptyMatrixException), () => new WordFinder(new List<string>()));
            Assert.Throws(typeof(ArgumentNullException), () => new WordFinder(null));
        }

        [Test]
        public void IfTheMatrixExceedsMaxSizeConstructorShouldFail()
        {
            var matrix = Enumerable.Repeat("abdcd", 129);
            Assert.Throws(typeof(MaxSizeExceededException), () => new WordFinder(matrix));

            var longString = String.Concat(Enumerable.Repeat('z', 129));
            matrix = Enumerable.Repeat(longString, 1);
            Assert.Throws(typeof(MaxSizeExceededException), () => new WordFinder(matrix));
        }

        [Test]
        public void IfTheMatrixDoesntHaveSameLengthLinesConstructorShouldFail()
        {
            var matrix = new List<string>() { "abcd", "abc" };
            Assert.Throws(typeof(UnevenRowLengthException), () => new WordFinder(matrix));
        }

        [TestCaseSource(typeof(MatrixAndWordStreamSamples))]
        public void FindShouldReturnTheTop10MostRepeatedWords(IEnumerable<string> matrix,
            IEnumerable<string> wordstream,
            bool checkEquivalent,
            bool checkCount,
            string testName,
            IEnumerable<string> expectedResult)
        {
            var wordFinder = new WordFinder(matrix);
            var findResult = wordFinder.Find(wordstream);

            if (checkEquivalent)
                CollectionAssert.AreEquivalent(expectedResult, findResult, $"{testName} - The result set does not match with the excpected result.");
            if (checkCount)
                Assert.AreEqual(expectedResult.Count(), findResult.Count(), $"{testName} - The result set does not match with the excpected result.");
        }
    }

    public class MatrixAndWordStreamSamples : IEnumerable
    {
        private static IEnumerable<IFindTestData> FindTestData
        {
            get
            {
                yield return new GivenExampleData();
                yield return new NoWordsAreFoundTestData();
                yield return new MoreThan10WordsAreFoundData();
                yield return new WordstreamWithRepeatedWordsData();
            }

        }

        public IEnumerator GetEnumerator()
        {
            foreach (var data in FindTestData)
            {
                yield return new object[] { data.Matrix, data.Wordstream, data.CheckEquivalent, data.CheckCount, data.Name, data.ExpectedResult };
            }
        }
    }

    /// <summary>
    /// Generic representation of data needed to test the Find method of WordFinder's class
    /// </summary>
    public interface IFindTestData
    {
        public string Name { get; }
        /// <summary>
        /// Matrix to find in
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Matrix { get; }
        /// <summary>
        /// Words to find
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Wordstream { get; }
        /// <summary>
        /// Expected find result
        /// </summary>
        /// <returns>List with the expected top 10 most repeated words</returns>
        public IEnumerable<string> ExpectedResult { get; }

        public bool CheckEquivalent { get; }

        public bool CheckCount { get; }
    }

    #region Data Samples

    public abstract class BaseTestData
    {
        public virtual string Name => this.GetType().Name;
        public virtual bool CheckEquivalent => true;
        public virtual bool CheckCount => true;
    }

    /// <summary>
    /// Contains the data given in the task example
    /// </summary>
    public class GivenExampleData : BaseTestData, IFindTestData
    {
        IEnumerable<string> IFindTestData.Matrix =>
            new List<string>()
            {
                "abcdc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy"
            };
        IEnumerable<string> IFindTestData.Wordstream =>
            new List<string>()
            {
                "chill",
                "cold",
                "wind"
            };

        IEnumerable<string> IFindTestData.ExpectedResult =>
            new List<string>()
            {
                "chill",
                "cold",
                "wind"
            };
    }

    /// <summary>
    /// Contains the data needed to test the no words founded case
    /// </summary>
    public class NoWordsAreFoundTestData : BaseTestData, IFindTestData
    {
        IEnumerable<string> IFindTestData.Matrix =>
            new List<string>()
            {
                "abcdc",
                "fgwio",
                "chill",
                "pqnsd",
                "uvdxy"
            };

        IEnumerable<string> IFindTestData.Wordstream =>
            new List<string>()
            {
                "warmth",
                "hot",
                "still"
            };

        /// <summary>
        /// An emtpy set should be expected if no words are found
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> IFindTestData.ExpectedResult => new List<string>();

    }

    /// <summary>
    /// The wordstream contains more than 10 words that are present in the matrix
    /// The result should only show 10 of them.
    /// There are 
    /// </summary>
    public class MoreThan10WordsAreFoundData : BaseTestData, IFindTestData
    {
        IEnumerable<string> IFindTestData.Matrix =>
            new List<string>()
            {
                "rcchillss",
                "alcoldxut",
                "iowindzni",
                "nuwarmthl",
                "jdhotyzll",
                "zzwetyyyy",
                "dryxxxxxx",
                "zzzzzsnow"
            };

        IEnumerable<string> IFindTestData.Wordstream => new List<string>()
            {
                "chill",
                "cold",
                "wind",
                "warmth",
                "hot",
                "still",
                "cloud",
                "sun",
                "rain",
                "wet",
                "dry",
                "snow"
            };

        /// <summary>
        /// An emtpy set should be expected if no words are found
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> IFindTestData.ExpectedResult => new List<string>()
            {
                "chill",
                "cold",
                "wind",
                "warmth",
                "hot",
                "still",
                "cloud",
                "sun",
                "rain",
                "wet"
            };

        public override bool CheckEquivalent => false;
    }




    /// <summary>
    /// The wordstream contains repeated words. 
    /// The result should only retrieve one of them.
    /// </summary>
    public class WordstreamWithRepeatedWordsData : BaseTestData, IFindTestData
    {
        IEnumerable<string> IFindTestData.Matrix => new List<string>()
            {
                "rcchillss",
                "alcoldxut",
                "iowindzni",
                "nuwarmthl",
                "jdhotyzll",
                "zzwetyyyy",
                "dryxxxxxx",
                "zzzzzsnow"
            };
        IEnumerable<string> IFindTestData.Wordstream =>
            new List<string>()
            {
                "chill",
                "chill",
                "cold",
                "cold",
                "wind",
                "cold",
                "warmth",
                "hot",
                "cold",
                "still",
                "cloud",
                "sun",
                "rain",
                "wet"
            };

        /// <summary>
        /// An emtpy set should be expected if no words are found
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> IFindTestData.ExpectedResult =>
            new List<string>()
            {
                "chill",
                "cold",
                "wind",
                "warmth",
                "hot",
                "still",
                "cloud",
                "sun",
                "rain",
                "wet"
            };
    }

    #endregion
}
