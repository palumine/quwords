﻿using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QuWords.Words.Test
{
    [TestFixture]
    public class WordFinderPerformanceTest
    {
        private const int MATRIX_MAX_SIZE = 64;


        private static readonly ILoggerFactory LoggerFactory =
            Microsoft.Extensions.Logging.LoggerFactory
                .Create(builder => builder
                    .AddSimpleConsole(config => config.TimestampFormat = "[HH:mm:ss.fff] ")
                );

        private static readonly ILogger<WordFinderPerformanceTest> Logger = LoggerFactory.CreateLogger<WordFinderPerformanceTest>();

        private IEnumerable<string> Matrix { get; set; }
        private IEnumerable<string> Wordstream { get; set; }

        private WordFinder WordFinder { get; set; }

        [OneTimeSetUp]
        public void Setup()
        {
            this.Matrix = GetMaxSizeMatrix();
            this.Wordstream = ExtractWordsFromMatrix(this.Matrix, 133120 * 2); //See comment in ExtractWordsFromMatrix
            this.WordFinder = new WordFinder(this.Matrix, LoggerFactory.CreateLogger<WordFinder>());
        }


        [Test]
        public void FindLargeWordstreamWithSequentialBatchFind()
        {
            TimingExecution(() =>
            {
                var logger = LoggerFactory.CreateLogger<SequentialBatchFind>();
                this.WordFinder.BatchFind = new SequentialBatchFind(logger);
                var result = this.WordFinder.Find(this.Wordstream);
                Assert.AreEqual(10, result.Count());
            }, "SequentialBatchFind Test"
            );
        }

        [Test]
        public void FindLargeWordstreamWithParallelForeachBatchFind()
        {
            TimingExecution(() =>
            {
                var logger = LoggerFactory.CreateLogger<ParallelForeachBatchFind>();
                this.WordFinder.BatchFind = new ParallelForeachBatchFind(logger);
                var result = this.WordFinder.Find(this.Wordstream);
                Assert.AreEqual(10, result.Count());
            }, "ParalleleBatchFind Test"
            );
        }

        [Test]
        public void FindLargeWordstreamWithSemaphoreSlimBatchFind()
        {
            TimingExecution(() =>
            {
                var logger = LoggerFactory.CreateLogger<SemaphoreSlimBatchFind>();
                this.WordFinder.BatchFind = new SemaphoreSlimBatchFind(logger);
                var result = this.WordFinder.Find(this.Wordstream);
                Assert.AreEqual(10, result.Count());
            }, "SemaphoreSlimBatchFind Test"
            );
        }

        public static void TimingExecution(Action action, string Name)
        {
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

            Logger.LogInformation(Name);
            timer.Start();
            action();
            timer.Stop();
            Logger.LogInformation($"Time in seconds.milliseconds to find the wordstream: {timer.Elapsed.Seconds}.{timer.Elapsed.Milliseconds}");
        }


        /// <summary>
        /// Extracts the amount <code>size</code> of words from the matrix
        /// It uses de result as wordstream to test the Find method
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private static IEnumerable<string> ExtractWordsFromMatrix(IEnumerable<string> matrix, int size)
        {
            var wordstream = new List<string>();
            ExtractWordsFromMatrix(matrix, size / 2, wordstream);
            var rotatedMatrix = new MatrixRotator().Rotate(matrix);
            ExtractWordsFromMatrix(rotatedMatrix, size / 2, wordstream);
            return wordstream;
        }


        /// <summary>
        /// The maximum amount of possible words in a 64x64 matrix are 133120 horizontally and 133120 
        /// vertically without considering that they could be duplicated
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="size"></param>
        /// <param name="wordstream"></param>
        private static void ExtractWordsFromMatrix(IEnumerable<string> matrix, int size, List<string> wordstream)
        {
            var line = 0;
            var wordLen = 1;
            var from = 0;
            var wordCount = 0;

            while (wordLen <= MATRIX_MAX_SIZE && wordCount <= size)
            {
                while (from + wordLen <= MATRIX_MAX_SIZE)
                {
                    while (line < MATRIX_MAX_SIZE)
                    {
                        wordstream.Add(matrix.ElementAt(line).Substring(from, wordLen));
                        wordCount++;
                        line++;
                    }
                    line = 0;
                    from++;
                }
                from = 0;
                wordLen++;
            }
        }

        private static IEnumerable<string> GetMaxSizeMatrix()
        {
            var matrix = new List<String>();

            for (var i = 0; i < MATRIX_MAX_SIZE; i++)
                matrix.Add(GetRandomRow());
            return matrix;
        }


        private static string GetRandomRow()
        {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[MATRIX_MAX_SIZE];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }
    }
}
